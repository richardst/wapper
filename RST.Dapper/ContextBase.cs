﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Data;

/*
 R S Taylor, March 2013
 Base library of data access methods for factory classes to implement. 
*/

namespace RST.Dapper
{
  public abstract class ContextBase : ContextProvider, IDisposable
  {
    public ContextBase() { }

    protected IEnumerable<T> ExecuteQuery<T>(string command, object parameters = null) where T : class
    {
      using (var context = base.Connection)
      {
        return context.Query<T>(command,
        param: parameters, commandType: CommandType.StoredProcedure, commandTimeout: 0);
      }
    }

    protected T ExecuteWithOutput<T>(string command, string outputName, object parameters = null)
    {
      var p = new DynamicParameters(parameters);
     // p.AddDynamicParams(parameters);
      p.Add(outputName, direction: ParameterDirection.Output, size: 1);  

      using (var context = base.Connection)
      {
        context.Execute(command,
        param: p, commandType: CommandType.StoredProcedure, commandTimeout: 0);

        return p.Get<T>(outputName);
      }
    }

    protected void ExecuteOnly(string command, object parameters = null)
    {
      using (var context = base.Connection)
      {
        context.Execute(command,
          param: parameters, commandType: CommandType.StoredProcedure);
      }
    }

    protected int ExecuteScalar(string command, object parameters = null)
    {
      using (var context = base.Connection)
      {
        var id = context.Query<int>(command, param: parameters,
        commandType: CommandType.StoredProcedure);

        return id.FirstOrDefault();
      }
    }


    protected int ExecuteScalarWithOutput<T>(string command, string outputName, int outputSize, out T returnObject, object parameters = null)
    {
      var p = new DynamicParameters(parameters);
      p.Add(outputName, direction: ParameterDirection.Output, size: outputSize);

      using (var context = base.Connection)
      {
        var id = context.Query<int>(command, param: p,
        commandType: CommandType.StoredProcedure);

        returnObject = p.Get<T>(outputName);

        return id.FirstOrDefault();
      }
    }


    //Defined for use with queries returning single types & related child collections within.
    protected T ExecuteQuery<T, T1>(string command, out IEnumerable<T1> secondary, object parameters = null) 
      where T : class //,new() where T1 : class
    {
      using (var context = base.Connection)
      {
        using (var multi = context.QueryMultiple(command, parameters, commandType: CommandType.StoredProcedure))
        {
          var query = multi.Read<T>().SingleOrDefault();
          secondary = multi.Read<T1>();

          return query;
        }
      }

    }


    public void Dispose()
    {
      if (base.Connection != null)
        base.Connection.Dispose();
    }
  }
}
