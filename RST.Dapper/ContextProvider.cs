﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace RST.Dapper
{
  public class ContextProvider : IDisposable
  {
    private DBContext context;

    private IDbConnection connection;
    protected internal IDbConnection Connection
    {
      get 
      { 
        if (context == null) { context = new DBContext(ConnectionString); }

        connection = context.Connection;
        
        return connection;
      }
      set { connection = value; }
    }

    public virtual string ConnectionString
    {
      get
      {
        return Properties.Settings.Default.ConnectionString;
      }
    }


    protected ContextProvider()
    {
      if (String.IsNullOrEmpty(ConnectionString))
        throw new FormatException("The connection string is not valid");

      //context = new DBContext(ConnectionString);
     
    }

    public void Dispose()
    {
      if (context != null)
        context.Dispose();
    }
  }
}
