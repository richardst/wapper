﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace RST.Dapper
{
  /// <summary>
  /// Responsible for creating & opening a database connection
  /// </summary>
  internal class DBContext : IDisposable
  {
    private string ConnectionString { get; set; }

    internal DBContext(string connectionString)
    {
      this.ConnectionString = connectionString;
    }

    internal IDbConnection Connection
    {
      get
      {
        try
        {
          var connection = new SqlConnection(ConnectionString);
          connection.Open();
          return connection;
        }
        catch (Exception ex)
        {
          throw ex;
        }
      }
    }

    public void Dispose()
    {
      if (Connection != null)
        Connection.Dispose();
    }
  }
}
