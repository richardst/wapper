I created this layer on top of Dapper (uncompiled as SqlMapper.cs in this project for debug purposes) to allow me to use Stored Procedures & return strongly typed objects in a single call.

Create a factory project with a base class inheriting from the Wapper project.

```
#!c#




public class FactoryBase : ContextBase
  {
    public override string ConnectionString
    {
      get
      {
        return base.ConnectionString;
      }
    }
  }

//Then implemented like this:

public class ReportFactory : FactoryBase
{
    public IEnumerable<MonthlyTotals> GetMonthlyTotals(string customerCode)
    {
      return ExecuteQuery<MonthlyTotals>(Commands.MonthlyTotalsDeliveryNote, new { CustomerCode = customerCode });
    }
}

```